
package PaqueteUsuarios;
import PaqueteVehiculos.Vehiculos;
import java.util.ArrayList;
import PaquetePersonas.Visitantes;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;


public class UsuariosResidentes extends Usuarios{
    
    /*Creamos un ArrayList vacio para que cada UsuarioResidente pueda registra cualquier cantidad de carros que posea*/
    private String pinDeAcceso;
    private ArrayList <Vehiculos> listaVehiculosRegistrados = new ArrayList();
    private ArrayList <Visitantes> listaVisitantesRegistrados = new ArrayList();
    private ArrayList <LocalDate> listaDiaVisitante = new ArrayList();
    private ArrayList <LocalTime> listaHoraVisitante = new ArrayList();
    //cidigo generado por visitante
    
    public UsuariosResidentes(){
        
    }
    
    public UsuariosResidentes(String nombreUsuario, String contrasenaUsuario, String pinDeAcceso, ArrayList listaVehiculosRegistrados, ArrayList listaVisitantesRegistrados, ArrayList listaDiaVisitante, ArrayList listaHoraVisitante){
        super(nombreUsuario, contrasenaUsuario);
        this.pinDeAcceso = pinDeAcceso;
        this.listaVehiculosRegistrados = listaVehiculosRegistrados;
        this.listaVisitantesRegistrados = listaVisitantesRegistrados;
        this.listaDiaVisitante = listaDiaVisitante;
        this.listaHoraVisitante = listaHoraVisitante;
    }
    
    /*Si el nuevo pin que desea cambiar es de tamano 4 y es diferente al pin existente, entonces se cambia el pin y retorna true, caso contrario retorna false*/
    public boolean cambiarPin(String nuevoPin){
        if(nuevoPin.length() == 4 && !(nuevoPin.equals(pinDeAcceso))){
            this.pinDeAcceso = nuevoPin;
            return true;
        }return false;
    }
    
    /*Tengo que verificar si el UsuarioResidente ya ha registrado un vehiculo con la misma matricula previamente*/
    public boolean registrarVehiculo(Vehiculos vehiculo){
        for(Object v: listaVehiculosRegistrados){
            if(v instanceof Vehiculos){
                Vehiculos v1 = (Vehiculos)v;
                if(v1.getMatricula().equals(vehiculo.getMatricula())){
                    listaVehiculosRegistrados.add(vehiculo);
                    return true;
                }
            }
        }return false;
    }
    
    
    /*verifico si el visitante ya se encuentra registrado, si no, lo agrego y retorno true, caso contrario no agrego y retorno false*/
    public boolean registrarVisitante(Visitantes visitante, LocalDate fechaIngresoVis, LocalTime horaIngresoVis){
        for(Object v: listaVisitantesRegistrados){
            if(v instanceof Visitantes){
                Visitantes v1 = (Visitantes)v;
                if(v1.getNombre().equals(visitante.getNombre())){
                    return false;
                }
            }
        }
        //Creo 3 ArrayLists que vn a coincidir con los datos e un solo visitante ingresado, por medio de la posicion.
        listaVisitantesRegistrados.add(visitante);
        listaDiaVisitante.add(fechaIngresoVis);
        listaHoraVisitante.add(horaIngresoVis);
        return true;
    }
    
    
    /*analizo la lista de visitantes del residente, si el visitante se encuentra, lo remueve y retorna verdadero, caso contrario retorna falso*/
    //Se eliminan los datos de los ArrayLists de hora y dia de ingreso en la misma posicion que estaba el visitante.
    public boolean eliminarVisitante(Visitantes visitante){
        int indice;
        for(Object v: listaVisitantesRegistrados){
            if(v instanceof Visitantes){
                Visitantes v1 = (Visitantes)v;
                if(v1.getNombre().equals(visitante.getNombre())){
                    indice = listaVisitantesRegistrados.indexOf(v1);
                    listaVisitantesRegistrados.remove(visitante);
                    listaDiaVisitante.remove(listaDiaVisitante.get(indice));
                    listaHoraVisitante.remove(listaHoraVisitante.get(indice));
                    return true;
                }
            }
        }return false;
    }
    
    public String getPinDeAcceso(){
        return pinDeAcceso;
    }
    
    public ArrayList getListaVisitantes(){
        return listaVisitantesRegistrados;
    }
    
    public ArrayList getListaDiaVisitante(){
        return listaDiaVisitante;
    }
    
    public ArrayList getListaHoraVisitante(){
        return listaHoraVisitante;
    }
    
    public ArrayList getListaVehiculosRegistrados(){
        return listaVehiculosRegistrados;
    }
}
