/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PaqueteUsuarios;

/**
 *
 * @author
 */
public class Usuarios {
    
    private String nombre;
    private String cedula;
    private String correo;
    private String nombreUsuario;
    private String contrasenaUsuario;
    
    public Usuarios(){
        
    }
    
    public Usuarios(String nombreUsuario, String contrasenaUsuario){
        this.nombreUsuario = nombreUsuario;
        this.contrasenaUsuario = contrasenaUsuario;
    }
    
    public String getNombreUsuario(){
        return nombreUsuario;
    }
    
    public String getContrasenaUsuario(){
        return contrasenaUsuario;
    }
    
    /*Creo que por si acaso hay que tener que imprimir la contrasena, hasta nuevo aviso*/
    public String toString(){
        return "Nombre del Usuario: "+nombreUsuario+" , Contrasena: "+contrasenaUsuario;
    }
}
