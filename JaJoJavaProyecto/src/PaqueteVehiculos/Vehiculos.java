/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PaqueteVehiculos;
import PaquetePersonas.Residentes;

/*
 */
public class Vehiculos {
    
    /*Cada vehiculo tiene una matricula y un propietario que es un residente*/
    
    String matricula;
    Residentes residente;
    
    public Vehiculos(){
        
    }
    
    public Vehiculos(String matricula, Residentes residente){
        this.matricula = matricula;
        this.residente = residente;
    }
    
    public String getMatricula(){
        return matricula;
    }
    
    /*los vehiculos tendran propietarios de clase Residente ya que el proceso de visitantes no incluye lectura de matricula de vehiculo*/
    
    public Residentes getPropietario(){
        return residente;
    }
    
    public String toString(){
        return "Matricula: "+matricula+", Propietario( "+residente+" )";
    }
    
}
